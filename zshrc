ZSHDIR=$HOME/.zsh

# Set history size to 1000 and setup paths for history files
HISTSIZE=1000
HISTFILE=$HOME/.zsh_history
SAVEHIST=1000

# Ignore duplicates in the zsh history file
setopt HIST_IGNORE_DUPS

# Set up the prompt
autoload -U colors && colors
PROMPT="%(!.%{$fg[red]%}%n.%{$fg[green]%}%n)%{$reset_color%}@%m:%2~ %# "

# Set up the right prompt
source $ZSHDIR/git_prompt.zsh

# OS Specific settings
unamestr=$(uname)
if [ "$unamestr" = "Darwin" ]; then
    # Enable colored ls output
    alias ls='ls -G'

    # Disable sshfs from creating apple double files and enable symlinks
    alias sshfs="sshfs -o noappledouble,follow_symlinks"

    # add terminfo directory (temp fix for old ncurses and tmux)
    export TERMINFO_DIRS=$TERMINFO_DIRS:$HOME/.local/share/terminfo
elif [ "$unamestr" = "Linux" ]; then
    # Enable colored ls output
    alias ls='ls --color=auto'
fi
unset unamestr

# Transmission aliases
alias tmsd="transmission-daemon"
alias tsmr="transmission-remote"

# autocomplete settings
zstyle :compinstall filename '~/.zshrc'
autoload -Uz compinit
compinit -u
setopt completealiases
zstyle ':completion:*' menu select
