Homebrew
=======

Install Homebrew:
```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Homebrew packages I always use:
* vim
* zsh
* tmux
* inetutils
* transmission
* ffmpeg
