# This file is sourced on all invocations of the shell, unless 
# the -f option is set. It should contain commands to set the
# command search path, plus other important environment variables.
# `.zshenv' should not contain commands that produce output or 
# assume the shell is attached to a tty.

# Set up Java path
export JAVA_HOME=$(/usr/libexec/java_home)

# Set up paths
export PATH="/usr/local/sbin:$PATH"
export PATH="$HOME/Library/Android/sdk/tools/bin:$PATH"
export PATH="$HOME/Library/Android/sdk/platform-tools:$PATH"

# Rust setup
source "$HOME/.cargo/env"

# pyenv setup
eval "$(pyenv init --path)"

# NVM settings
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
