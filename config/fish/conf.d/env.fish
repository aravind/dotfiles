# Environment variable settings
# Author: aravind

# Set up Java path
set -gx JAVA_HOME (/usr/libexec/java_home)

# Android dev tools
fish_add_path -g $HOME/Library/Android/sdk/tools/bin
fish_add_path -g $HOME/Library/Android/sdk/platform-tools

# pyenv
pyenv init - | source
goenv init - | source

