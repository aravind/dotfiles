# Right prompt settings
# Author: aravind

if status is-interactive
    function fish_right_prompt -d "Write out the right prompt"
        set -l normal (set_color normal)
        set -f git_state ""
        set -f git_status ""
        set -f git_branch (git_branch)

        if test -n "$git_branch"
            set -f git_state (git_state)
            set -f git_status (git_status)
        end

        set -l r_prompt "$git_state$git_status$git_branch"
        if test -n "$r_prompt"
            printf "%s" $r_prompt
        end
    end
end
