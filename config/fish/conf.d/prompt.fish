# Prompt settings
# Author: aravind
#
# Modified version of the default fish prompt

if status is-interactive
    function fish_prompt -d "Write out the prompt"
        set -l last_status $status
        set -l user_color brgreen
        set -l color_cwd $fish_color_cwd
        set -l suffix_color brblue
        set -l suffix '❯'

        # Color the prompt differently when we're root
        if functions -q fish_is_root_user; and fish_is_root_user
            if set -q fish_color_cwd_root
                set user_color $fish_color_cwd_root
                set color_cwd $fish_color_cwd_root
            end
            set suffix '#'
        end

        if test "$last_status" -ne 0
            set suffix_color red
        end

        set -l prompt (set_color $user_color)$USER(set_color normal)@(hostname -s)
        set prompt "$prompt "(set_color $color_cwd)(prompt_pwd)(set_color normal)
        set prompt "$prompt "(set_color $suffix_color)"$suffix"(set_color normal)" "

        printf "%s" $prompt
    end
end
